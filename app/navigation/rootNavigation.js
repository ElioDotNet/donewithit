import React from 'react';

// Si inserisce un riferimento da usare nell'App.js
export const navigationRef = React.createRef();

const navigate = ( name, params) => // sono i parametri del metodo navigate
  navigationRef.current?.navigate(name, params);

export default {
  navigate,
};
