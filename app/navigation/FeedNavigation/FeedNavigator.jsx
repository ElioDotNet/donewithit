import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ListingsScreen from '../../screens/ListingsScreen';
import ListingDetailsScreen from '../../screens/ListingDetailsScreen';

const Stack = createStackNavigator();
/* Mettendo questi component all'interno di Stack.Navigator questi avranno a disposizione
  le proprietà di navigazione(es root, navigation) */
const FeedNavigator = () => (
  <Stack.Navigator mode='modal' screenOptions={{ headerShown: false }}>
    <Stack.Screen name='Listings' component={ListingsScreen} />
    <Stack.Screen name='ListingDetails' component={ListingDetailsScreen} />
  </Stack.Navigator>
);

export default FeedNavigator;
