import { DefaultTheme } from '@react-navigation/native';
import colors from '../config/colors';

// Vengono importati tutti i valori di default da DefaultTheme sovrascrivendo quelli che ci interessano
export default {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    background: colors.white,
  },
};
