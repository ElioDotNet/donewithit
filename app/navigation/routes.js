export default Object.freeze({  // con Object.freeze non si può modificare l'oggetto
  LISTING_DETAILS: 'ListingDetails',
  LISTING_EDIT: 'ListingEdit',
  LOGIN: 'Login',
  MESSAGES: 'Messages',
  REGISTER: 'Register',
});
