import { create } from 'apisauce'; // con apisauce si può evitare di usare il blocco try e catch
import cache from '../utility/cache';
import authStorage from '../auth/storage';
import settings from '../config/settings';

const apiClient = create({
  baseURL: settings.apiUrl,
});

// Si prende il token e si aggiunge all'header
apiClient.addAsyncRequestTransform(async (request) => {
  const authToken = await authStorage.getToken();
  if (!authToken) return;
  request.headers['x-auth-token'] = authToken;
});

const get = apiClient.get;  // riferimento al metodo get
// Si ridefinisce il metotodo get
apiClient.get = async (url, params, axiosConfig) => {
  const response = await get(url, params, axiosConfig);

  if (response.ok) {
    cache.store(url, response.data);
    return response;
  }
  // La response fallisce e si restituisce il contenuto della cache
  const data = await cache.get(url);
  return data ? { ok: true, data } : response;
};

export default apiClient;
