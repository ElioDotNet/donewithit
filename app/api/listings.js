import client from './client';

const endpoint = '/listings';

const getListings = () => client.get(endpoint);

export const addListing = (listing, onUploadProgress) => {
  // Quando si crea un FormData ApiSauce o Axios imposta il content-type = multipart/form-data per invio files
  const data = new FormData();
  data.append('title', listing.title);
  data.append('price', listing.price);
  data.append('categoryId', listing.category.value);
  data.append('description', listing.description);

  listing.images.forEach((image, index) =>
    data.append('images', {
      name: 'image' + index,
      type: 'image/jpeg',
      uri: image,
    })
  );

  if (listing.location)
    data.append('location', JSON.stringify(listing.location));

  return client.post(endpoint, data, {
    onUploadProgress: (progress) =>
      onUploadProgress(progress.loaded / progress.total), // passando onUploadProgress si restituisce al chiamante l'avanzamento dell'upload
  });
};

export default {
  addListing,
  getListings,
};
