import { StyleSheet } from 'react-native';

import Constants from 'expo-constants';

const styles = StyleSheet.create({
    screen: {
      paddingTop: Constants.statusBarHeight, // usando Constants di Expo si evita di controllare il SO tramite Platform
      flex: 1, // serve per fare occupare al component l'intero schermo
    },
    view: {
      flex: 1, // serve per evitare lo scrolling in basso
    },
  });

  export default styles;