import React from 'react';
import { SafeAreaView, View } from 'react-native';

import styles from './styles';

export default function Screen({ children, style }) {
  // L'array serve per poter passare lo stile personalizzato
  return (
    // La View interna serve per inserire un padding left che se passato nei parametri, non funziona(versione attuale)
    <SafeAreaView style={[styles.screen, style]}>
      {/*  Aggiungo allo stile quello passato al component */}
      <View style={[styles.view, style]}>{children}</View>
    </SafeAreaView>
  );
}
