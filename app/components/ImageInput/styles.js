import { StyleSheet } from 'react-native';

import colors from '../../config/colors';

const styles = StyleSheet.create({
    container: {
      alignItems: "center",
      backgroundColor: colors.light,
      borderRadius: 15,
      height: 100,
      justifyContent: "center",
      marginVertical: 10,
      overflow: "hidden",
      width: 100,
    },
    image: {
      height: "100%",
      width: "100%",
    },
  });