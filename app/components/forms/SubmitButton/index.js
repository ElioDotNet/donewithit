// Serve per evitare dupplicazione di percorsi quando si usa questo component, facendo riferimento alla cartella
import SubmitButton from './SubmitButton';
export default SubmitButton;
/* Se i component fossero tutti dentro la cartella form si potrebbe usa questa sintassi
export { default as SubmitButton } from './SubmitButton'; */