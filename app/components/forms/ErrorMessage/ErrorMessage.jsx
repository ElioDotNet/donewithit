import React from 'react';

import Text from '../../Text';
import styles from './styles';

export default function ErrorMessage({ error, visible }) {
  if (!visible || !error) return null;

  return <Text style={styles.error}>{error}</Text>;
}

