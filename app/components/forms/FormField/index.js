// Serve per evitare dupplicazione di percorsi quando si usa questo component, facendo riferimento alla cartella
import FormField from './FormField';
export default FormField;
/* Se i component fossero tutti dentro la cartella form si potrebbe usa questa sintassi
export { default as FormField } from './FormField'; */