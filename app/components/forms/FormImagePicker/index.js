// Serve per evitare dupplicazione di percorsi quando si usa questo component, facendo riferimento alla cartella
import FormImagePicker from './FormImagePicker';
export default FormImagePicker;
/* Se i component fossero tutti dentro la cartella form si potrebbe usa questa sintassi
export { default as FormImagePicker } from './FormImagePicker'; */