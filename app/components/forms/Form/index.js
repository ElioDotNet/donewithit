// Serve per evitare dupplicazione di percorsi quando si usa questo component, facendo riferimento alla cartella
import Form from './Form';
export default Form;
/* Se i component fossero tutti dentro la cartella form si potrebbe usa questa sintassi
export { default as Form } from './Form'; */