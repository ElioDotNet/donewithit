import React from 'react';
import { Text } from 'react-native';

import defaultStyles from '../../config/styles';

// Con lo style è possibile sotituire lo stile con qullo passato in input
function AppText({ children, style, ...otherProps }) { // restanti proprietà
  return (
    <Text style={[defaultStyles.text, style]} {...otherProps}>
      {children}
    </Text>
  );
}

export default AppText;
