import React from 'react';
import { View } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo'; // contiene informazioni riguardanti la connessione

import Text from '../Text';

import styles from './styles';

export default function OfflineNotice() {
  const netInfo = useNetInfo(); // con questo hook si hanno info sulla connessione

  if (netInfo.type !== 'unknown' && netInfo.isInternetReachable === false)
    return (
      <View style={styles.container}>
        <Text style={styles.text}>No Internet Connection</Text>
      </View>
    );

  return null;
}
