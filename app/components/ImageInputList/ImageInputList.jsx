import React, { useRef } from 'react';
import { View, ScrollView } from 'react-native';

import ImageInput from '../ImageInput';

import styles from './styles';

export default function ImageInputList({ imageUris = [], onRemoveImage, onAddImage }) {
  const scrollView = useRef();

  return (
   // Mettendo lo ScrollView dentro una View si evita che occupi tutta la dimensione disponibile 
    <View>
       {/* Inserendo dentro ScrollView si abilita lo scroll orizzontale(default verticale) */}
       {/* Quando cambia la dimensione si scrolla verticalmente */}
      <ScrollView
        ref={scrollView}
        horizontal
        onContentSizeChange={() => scrollView.current.scrollToEnd()}
      >
        <View style={styles.container}>
          {imageUris.map((uri) => (
            <View key={uri} style={styles.image}>
              <ImageInput
                imageUri={uri}
                onChangeImage={() => onRemoveImage(uri)}
              />
            </View>
          ))}
          <ImageInput onChangeImage={(uri) => onAddImage(uri)} />
        </View>
      </ScrollView>
    </View>
  );
}
