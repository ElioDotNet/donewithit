import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import colors from '../../config/colors';
import styles from './styles';

export default function AppButton({ title, onPress, color = 'primary' }) {
  return (
    // Sostituiamo il backgroundColor con il colors la cui propietà passiamo al component
    <TouchableOpacity
      style={[styles.button, { backgroundColor: colors[color] }]}
      onPress={onPress}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
}
