import React from 'react';
import { View, KeyboardAvoidingView, Platform } from 'react-native'; // con Platform si può identificare SO dove gira l'app
import { Image } from 'react-native-expo-image-cache';

import ContactSellerForm from '../../components/ContactSellerForm';
import ListItem from '../../components/lists/ListItem';
import Text from '../../components/Text';

import styles from './styles';

export default function ListingDetailsScreen({ route }) {
  const listing = route.params;

  return (
    <KeyboardAvoidingView
      behavior='position'
      keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 100}
    >
      <Image
        style={styles.image}
        preview={{ uri: listing.images[0].thumbnailUrl }}
        tint='light'
        uri={listing.images[0].url}
      />
      <View style={styles.detailsContainer}>
        <Text style={styles.title}>{listing.title}</Text>
        <Text style={styles.price}>${listing.price}</Text>
        <View style={styles.userContainer}>
          <ListItem
            image={require('../../assets/elio.jpg')}
            title='Elio Ferrero'
            subTitle='5 Listings'
          />
        </View>
        <ContactSellerForm listing={listing} />
      </View>
    </KeyboardAvoidingView>
  );
}
