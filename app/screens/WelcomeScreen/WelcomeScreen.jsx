import React from 'react';
import { ImageBackground, View, Image, Text } from 'react-native';

import Button from '../../components/Button';
import routes from '../../navigation/routes';

import styles from './styles';

export default function WelcomeScreen({ navigation }) {
  // Require si usa per leggere le immagini statiche, mentre per immagini dalla rete si usa uri, ma occorre specificare le dimensioni
  return (
    <ImageBackground
      blurRadius={10}
      style={styles.background}
      source={require('../../assets/background.jpg')}
    >
      <View style={styles.logoContainer}>
        <Image
          style={styles.logo}
          source={require('../../assets/logo-red.png')}
        />
        <Text style={styles.tagline}>Sell What You Don't Need</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <Button
          title='Login'
          onPress={() => navigation.navigate(routes.LOGIN)}
        />
        <Button
          title='Register'
          color='secondary'
          onPress={() => navigation.navigate(routes.REGISTER)}
        />
      </View>
    </ImageBackground>
  );
}
