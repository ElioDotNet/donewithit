import React, { useState } from 'react';
import { Image } from 'react-native';
import * as Yup from 'yup'; // si usa per la validazione del form

import Screen from '../../components/Screen';
import Form from '../../components/forms/Form';
import FormField from '../../components/forms/FormField';
import ErrorMessage from '../../components/forms/ErrorMessage';
import SubmitButton from '../../components/forms/SubmitButton';

import authApi from '../../api/auth';
import useAuth from '../../auth/useAuth';

import styles from './styles';

// Regole di validazione del Form
const validationSchema = Yup.object().shape({
  // Con label in caso di errore si visualizza il campo con quel nome
  email: Yup.string().required().email().label('Email'),
  password: Yup.string().required().min(4).label('Password')
});

export default function LoginScreen() {
  const auth = useAuth();
  const [loginFailed, setLoginFailed] = useState(false);

  const handleSubmit = async ({ email, password }) => {
    const result = await authApi.login(email, password);
    if (!result.ok) return setLoginFailed(true);
    setLoginFailed(false);
    auth.logIn(result.data);
  };

  return (
    <Screen style={styles.container}>
      <Image style={styles.logo} source={require('../../assets/logo-red.png')} />

      <Form
        initialValues={{ email: '', password: '' }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <ErrorMessage
          error='Invalid email and/or password.'
          visible={loginFailed}
        />
        <FormField
          autoCapitalize='none'
          autoCorrect={false}
          icon='email'
          keyboardType='email-address'
          name='email'
          placeholder='Email'
          textContentType='emailAddress' // solo su IOS
        />
        <FormField
          autoCapitalize='none'
          autoCorrect={false}
          icon='lock'
          name='password'
          placeholder='Password'
          secureTextEntry
          textContentType='password' // solo su IOS
        />
        <SubmitButton title='Login' />
      </Form>
    </Screen>
  );
}
