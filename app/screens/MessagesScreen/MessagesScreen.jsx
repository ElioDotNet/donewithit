import React, { useState } from 'react';
import { FlatList } from 'react-native';

import Screen from '../../components/Screen';
import ListItem from '../../components/lists/ListItem';
import ListItemSeparator from '../../components/lists/ListItemSeparator';
import ListItemDeleteAction from '../../components/lists/ListItemDeleteAction';

const initialMessages = [
  {
    id: 1,
    title: 'Elio Ferrero',
    description: 'Hey! Is this item still available?',
    image: require('../../assets/elio.jpg'),
  },
  {
    id: 2,
    title: 'Elio Ferrero',
    description:
      "I'm interested in this item. When will you be able to post it?",
    image: require('../../assets/elio.jpg'),
  },
];

export default function MessagesScreen() {
  const [messages, setMessages] = useState(initialMessages);
  const [refreshing] = useState(false);

  const handleDelete = (message) => {
    // Delete the message from messages
    setMessages(messages.filter((m) => m.id !== message.id));
  };

  return (
    <Screen>
      <FlatList
        data={messages}
        keyExtractor={(message) => message.id.toString()} // keyExtractor deve essere una chiava univoca
        renderItem={({ item }) => (
          <ListItem
            title={item.title}
            subTitle={item.description}
            image={item.image}
            onPress={() => console.log('Message selected', item)}
            renderRightActions={() => (
              <ListItemDeleteAction onPress={() => handleDelete(item)} />
            )}
          />
        )}
        ItemSeparatorComponent={ListItemSeparator} // component per creare i separatore(si mette il riferimento)
        refreshing={refreshing}
        onRefresh={() => { // simulazione chiamata backend al refresh
          setMessages([
            {
              id: 2,
              title: 'T2',
              description: 'D2',
              image: require('../../assets/elio.jpg'),
            },
          ]);
        }}
      />
    </Screen>
  );
}
