import Constants from 'expo-constants';

const settings = {
  dev: {
    apiUrl: 'http://192.168.0.171:9000/api',
  },
  staging: {
    apiUrl: 'http://192.168.0.171:9000/api',
  },
  prod: {
    apiUrl: 'http://192.168.0.171:9000/api',
  },
};

const getCurrentSettings = () => {
  if (__DEV__) return settings.dev; // __DEV__ variabile globale di React Native che indica se siamo in ambiente locale
  if (Constants.manifest.releaseChannel === 'staging') return settings.staging; // parametro passato quando si pubblica la app con Expo
  return settings.prod; // ambiente di produzione
};

export default getCurrentSettings();
